print("**********Bienvenido al Sistema de Caculo de Aguinaldo by Jose Luis Lopez*********")

nombre = input("Digite su nombre: ")
dui = input("Digite su numero de Dui: ")
nit = input("Digite su numero de Nit: ")

sueldo_neto = float(input("Digite por favor su sueldo neto al Mes: $"))

menu = int(
    input("Escoja una opcion para calculo de aguinaldo: 1-años 2-meses 3-dias: "))

if menu == 1:
    tiempo_laboral = int(input("Digite cuanto años lleva laborando: "))
    if tiempo_laboral >= 1 or tiempo_laboral < 3:
        aguinaldo = (sueldo_neto / 30) * 15
        print("Su aguinaldo a recibir es de : $", aguinaldo)
    elif tiempo_laboral >= 3 or tiempo_laboral < 10:
        aguinaldo = (sueldo_neto / 30) * 19
        print("Su aguinaldo a recibir es de : $", aguinaldo)
    elif tiempo_laboral >= 10:
        aguinaldo = (sueldo_neto / 30) * 21
        print("Su aguinaldo a recibir es de : $", aguinaldo)

if menu == 2:
    tiempo_laboral = int(input("Digite cuanto meses lleva laborando: "))
    if tiempo_laboral >= 1 and tiempo_laboral <= 12:
        porcentaje = 0.041
        dia_prop = porcentaje * (sueldo_neto / 30)
        aguinaldo = dia_prop * (sueldo_neto / 30)
        print("Aguinaldo proporcional a recibir: $", aguinaldo)

if menu == 3:
    tiempo_laboral = int(input("Digite cuanto dias lleva laborando: "))
    print("lo siento no cumples con los requisitos minimos para recibir aguinaldo")
